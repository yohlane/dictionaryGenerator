## DictionaryGenerator

Small Python tool to create custom dictionary for bruteforce

```
usage: dictionaryGenerator.py [-h] [-o OUTPUTFILE] [-l LENGTH] [-p PATTERN] -c
                              CHARSET

optional arguments:
  -h, --help     show this help message and exit
  -o OUTPUTFILE  Output File
  -l LENGTH      Generated word length
  -p PATTERN     Format Pattern to use for the generation (Default '{0}')
                  Extended version of PEP 3101
                  You can select {} elements like in arrays
                  Following additional conversions have been added
                   * l: convert to string and low case
                   * u: convert to string and up case
                   * c: capitalize the string
                  Add more in ExtendedFormatter.py
                 
                  Example: inut='hello' "{0[0]\!u}**{0[1:]}"
                   "H**ello"
  -c CHARSET     Characters to use for the generation
```

## Requirements
Python 3

## Author
@yohlane

 **Free Software, Hell Yeah!**