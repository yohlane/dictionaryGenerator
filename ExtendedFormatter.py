from string import Formatter
import _string

class ExtendedFormatter(Formatter):
    """An extended format string formatter

    Formatter with extended conversion symbol
    """
    def convert_field(self, value, conversion):
        """ Extend conversion symbol
        Following additional symbol has been added
        * l: convert to string and low case
        * u: convert to string and up case
        * c: capitalize the string

        default are:
        * s: convert with str()
        * r: convert with repr()
        * a: convert with ascii()
        """

        if conversion == "u":
            return str(value).upper()
        elif conversion == "l":
            return str(value).lower()
        elif conversion == "c":
            return str(value).capitalize()
        # Do the default conversion or raise error if no matching conversion found
        super(ExtendedFormatter, self).convert_field(value, conversion)

        # return for None case
        return value
    def get_field(self, field_name, args, kwargs):
        #Based on original code
        first, rest = _string.formatter_field_name_split(field_name)

        obj = self.get_value(first, args, kwargs)

        # loop through the rest of the field_name, doing
        #  getattr or getitem as needed
        for is_attr, i in rest:
            if is_attr:
                obj = getattr(obj, i)
            else:
                # Modifications
                ##############################
                if isinstance(i, int):
                    obj = obj[i]
                else:
                    start,end,*step =[ int(x) if x else x for x in i.split(':')]

                    if start=="":
                        start=0
                    if end=="":
                        end=len(obj)

                    if len(step)==0:
                        step=1
                    else:
                        step=step[0]
                    if step=="":
                        step=1
                    elif step < 0:
                        t=start
                        start=end-1
                        end=t-1
                    
                    ret=""
                    for j in range(start,end,step):
                        ret+=obj[j]
                    obj=ret
                ##############################

        return obj, first

myformatter = ExtendedFormatter()