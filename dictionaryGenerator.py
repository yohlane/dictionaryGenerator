#!/usr/bin/env python

import string
import itertools
import sys
import argparse
from argparse import RawTextHelpFormatter
from ExtendedFormatter import ExtendedFormatter  

parser = argparse.ArgumentParser(description='Small Python tool to create custom dictionary for bruteforce.', formatter_class=RawTextHelpFormatter)
parser.add_argument('-o', dest='outputFile',default='dictionary.out', help='Output File')
parser.add_argument('-l', dest='length', default=5, help='Generated word length')
parser.add_argument('-p', dest='pattern', default='{0}', 
    help='Format Pattern to use for the generation (Default \'{0}\')\n\
 Extended version of PEP 3101\n\
 You can select {} elements like in arrays\n\
 Following additional conversions have been added\n\
  * l: convert to string and low case\n\
  * u: convert to string and up case\n\
  * c: capitalize the string\n\
 Add more in ExtendedFormatter.py\n\n\
 Example: inut=\'hello\' "{0[0]\!u}**{0[1:]}"\n\
  "H**ello"')
parser.add_argument('-c', dest='charSet', required=True, help='Characters to use for the generation')

args = parser.parse_args()
args.length = int(args.length)

myformatter = ExtendedFormatter()

with open(args.outputFile,'w') as f:
    for p in itertools.product(args.charSet,repeat=args.length):
        f.write( myformatter.format(args.pattern, ''.join(p)) + "\n")